<?php
/**
 *       @file  WafConnector.php
 *      @brief  
 *
 *      Quttera WAF connector
 *
 *     @author  Quttera Team (support@quttera.com)
 *
 *   @internal
 *     Created  24/06/18
 *     Company  Quttera LTD
 *   Copyright  Copyright (c) 2018, Quttera LTD
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

$WAF_LOG_FILE_PATH="waf.error";
$WAF_LOG_ACCESS_FILE_PATH="access";
$WAF_LOG_VERDICT_FILE_PATH="verdict";

$WAF_REQUEST_TIMEOUT=5; //5 seconds
$WAF_NODE_PORT=443;
$WAF_CONNECTOR_VERSION="1.0.6";
$WAF_CONTROL_TEST_SOURCE=FALSE;

/* $WAF_MODE= "ON"; //"OFF" */


/*
 * configuration parameters
 */
define("WAF_MODE","mode");
define("WAF_ADDRESS","address");
define("WAF_PORT","port");
define("WAF_DEFAULT_DOMAIN","domain");
define("WAF_TIMEOUT","timeout");
define("WAF_LICENSE","license");
define("WAF_CONTROL_ADDR","control");
define("WAF_CONFIG_INI","waf.ini");
define("WAF_CONTROL","347FC5458179868989E8195C8D4BB213");

define("WAF_VERDICT_TIME", "vtime");

/*
 * supported WAF modes
 */
define("WAF_MODE_ON","online");
define("WAF_MODE_OFF","offline");
define("WAF_MODE_PASSIVE","passive");

date_default_timezone_set('UTC');
$qtr_waf_date = date("F j, Y, g:i a");
$qtr_waf_file_date = date('_d_m_Y');

$qtr_waf_config_file = sprintf("%s/%s",dirname(__FILE__),WAF_CONFIG_INI);

/*
 * 0 - no debug output
 * 1 - output to log file
 * 2 - output to log file and stdout
 */
$QTR_WAF_DEBUG=0;
//error_reporting(E_ALL);
$QTR_WAF_DEBUG_LOG_FILE=sprintf("%s/waf%sdebug.log", dirname(__FILE__), $qtr_waf_file_date);


/*
 * Cure datastructure, every entry array(file path, pattern, replacement")
 */
$qtr_waf_curelist = array(
    /* array( $path, $pattern, $replacement) */
);


function WAF_safe_get_value($array, $key, $default)
{
    if(array_key_exists($key, $array)){
        return $array[$key];
    }

    return $default;
}


function WAF_safe_read($path){
    $output = NULL;
    $handle = fopen($path,"r+");
    if(flock($handle, LOCK_EX)) {
        $output = fread($handle, filesize($path));
        flock($handle, LOCK_UN);
    }
    fclose($handle);
    return $output;
}


function WAF_safe_write($fbody, $path){
    $output = NULL;
    $handle = fopen($path,"w+");
    if(flock($handle, LOCK_EX)) {
        fwrite($handle, $fbody, strlen($fbody));
        flock($handle, LOCK_UN);
    }
    fclose($handle);
    return $output;
}


function WAF_find_match($istring, $pattern){
    $match = preg_match("/" . $pattern . "/m", $istring, $group);

    if($match > 0 ){
        return $group[0];
    }
    return FALSE;
}


function WAF_cure($istring, $match, $replacement){
    return  str_replace($match, $replacement, $istring); 
}


function WAF_cure_files(){
    global $qtr_waf_curelist;

    foreach($qtr_waf_curelist as $entry){
        $path = $entry[0];
        $pattern = $entry[1];
        $replacement = $entry[2];
        $fbody = WAF_safe_read($path);
        if(!$fbody){
            continue;
        }
        $match = WAF_find_match($fbody,$pattern);
        if($match == FALSE){
            continue;
        }

        $fbody = WAF_cure($fbody,$match, $replacement);
        WAF_safe_write($fbody, $path);
    }
}

function WAF_run_cure($possibility){
    $r = rand();
    if( $r % $possibility == 0 ){
        WAF_cure_files();
    }
}



function WAF_CleanLogs()
{
    $c = rand(0,100);
    if( $c != 59)
    {
        /* 
         * trik to run entire code only once in a while
         */
        return;
    }

    $secs_in_month = 30 * 24 * 60 * 60;
    $lt = time() - $secs_in_month;
    $files_list = scandir(dirname(__FILE__));
    foreach($files_list as $file)
    {
        if( strstr($file,".log") != FALSE )
        {
            /* 
             * this is log file 
             */
            $file_name = dirname(__FILE__) . DIRECTORY_SEPARATOR . $file;

            $ctime = filectime( $file_name );

            if($ctime == FALSE)
            {
                /* access error */
                continue;
            }
            if( $ctime < $lt )
            {
                /*
                 * This is too old log file, removing it
                 */
                @unlink($file_name);
            }
        }
    }
}

/*
 * Supported WAF actions:
    * "BLOCK"
    * "DROP"
    * "PASS"
    * "REDIRECT"
 */

function WAF_log_access( $params )
{
    global $WAF_LOG_ACCESS_FILE_PATH, $qtr_waf_date, $qtr_waf_file_date;

    //var_dump($params);

    $query = substr($params['request_params'],0,1024);
    $query = preg_replace('/[\r\n]/','', $query);

    $log_line = sprintf("[%s] %s %s %s/%s %s [%s]\r\n",
                        $qtr_waf_date,
                        WAF_safe_get_value($_SERVER, "REMOTE_ADDR",     "[NULL]"),
                        WAF_safe_get_value($_SERVER, "REQUEST_METHOD",  "[NULL]"),
                        WAF_safe_get_value($_SERVER, "SERVER_NAME",     "[NULL]"),
                        WAF_safe_get_value($_SERVER, "REQUEST_URI",     "[NULL]"),
                        WAF_safe_get_value($_SERVER, "HTTP_USER_AGENT", "[NULL]"),
                        $query);

    $path = sprintf("%s/%s%s.log",
                    dirname(__FILE__),
                    $WAF_LOG_ACCESS_FILE_PATH,
                    $qtr_waf_file_date);

    @file_put_contents($path, $log_line, FILE_APPEND | LOCK_EX);
}

/*
 * log error to WAF log file
 */
function WAF_log_error($qtr_waf_error){
    global $WAF_LOG_FILE_PATH, $qtr_waf_date, $qtr_waf_file_date;
    $path = sprintf("%s/%s%s.log",
                    dirname(__FILE__),
                    $WAF_LOG_FILE_PATH,
                    $qtr_waf_file_date);

    @file_put_contents($path, "[$qtr_waf_date] $qtr_waf_error\r\n", FILE_APPEND | LOCK_EX);
}

/*
 * log verdict to WAF log file
 */
function WAF_log_verdict($qtr_waf_action, $qtr_waf_rule, $qtr_waf_redirect)
{
    global $WAF_LOG_VERDICT_FILE_PATH, $qtr_waf_date, $qtr_waf_file_date;

    $line = $qtr_waf_date . " WAF " . $qtr_waf_action . " rule [" . $qtr_waf_rule . "] [" .
            WAF_safe_get_value($_SERVER, "REQUEST_METHOD", "[NULL]") . 
            "://" . 
            WAF_safe_get_value($_SERVER, "SERVER_NAME", "[NULL]") .
            "/" .  
            WAF_safe_get_value($_SERVER, "REQUEST_URI", "[NULL]") . 
            "]" . 
            "\r\n";

    $path = sprintf("%s/%s%s.log",
                    dirname(__FILE__),
                    $WAF_LOG_VERDICT_FILE_PATH,
                    $qtr_waf_file_date);

    WAF_log_debug("Write verdict to $path");
    @file_put_contents($path, $line, FILE_APPEND | LOCK_EX);
}

/*
 * DEBUG log function, to be removed for production
 */
function WAF_log_debug($exp){
    global $QTR_WAF_DEBUG, $QTR_WAF_DEBUG_LOG_FILE;
    if( $QTR_WAF_DEBUG <= 0 ){
        return;
    }

    $bt = debug_backtrace();
    $caller = array_shift($bt);
    $cfile = basename($caller['file']);
    $cline = $caller['line'];

    if( $QTR_WAF_DEBUG >= 1 ){
        $output = print_r($exp, true);
        @file_put_contents($QTR_WAF_DEBUG_LOG_FILE, "$cfile:$cline\t$output\r\n", FILE_APPEND);
    }

    if( $QTR_WAF_DEBUG >= 2 ){
        print_r($exp);
        echo "\r\n";
    }
}

/*
 * returns default configuration if config file not found
 */
function WAF_get_default_configuration()
{
    global $WAF_NODE_PORT, $WAF_REQUEST_TIMEOUT;

    $def_config = array();
    $def_config[WAF_MODE] = WAF_MODE_OFF;
    $def_config[WAF_ADDRESS] = "";
    $def_config[WAF_PORT]=$WAF_NODE_PORT;
    $def_config[WAF_TIMEOUT]=$WAF_REQUEST_TIMEOUT;
    $def_config[WAF_LICENSE]="";
    $def_config[WAF_CONTROL_ADDR]="";
    return $def_config;
}


/**
 * @brief       store configuration in an INI file
 * @param[in]   $qtr_waf_data - associative array to store
 * @param[in]   $path - file path to store the configuration
 * @return      true on success and false on failure
 */
function WAF_store_config($qtr_waf_data, $path)
{
    WAF_log_debug("Overwriting configuration file: $path");

    if(file_exists($path)){
        if(!unlink($path)){
            WAF_log_debug("Failed to remove config file $path. Error: " . var_export(error_get_last(), true));
            #return false;
        }
    }

    $handle = fopen($path, 'w+');
    if($handle == FALSE ){
        WAF_log_debug("Failed to open configuration file to overwrite. Error: " . var_export(error_get_last(), true));
        return false;
    }

    foreach ($qtr_waf_data as $key => $value){
        fwrite($handle, "$key=$value\r\n");
    }

    fflush($handle);
    fclose($handle);
    return true;
}


/*
 * Configuration loader procedure
 * returns array of configuration parameters
 */
function WAF_load_config($path)
{

    if(!file_exists($path)){
        WAF_log_debug("Failed to locate $path, load default configuration");
        return WAF_get_default_configuration();
    }
    /*
     * read configuration
     */
    WAF_log_debug("Loading configuration from $path");
    $ini_array = parse_ini_file($path);
    WAF_log_debug("Loaded config:");
    WAF_log_debug($ini_array);
    /*
     * validate all required fields exists
     */
    if(!array_key_exists(WAF_ADDRESS,$ini_array)){
        /*
         * nothing todo without address
         */
        WAF_log_debug("Failed to locate address in provided configuration file. Return default config.");
        return WAF_get_default_configuration();
    }

    if(!array_key_exists(WAF_LICENSE,$ini_array)){
        /*
         * nothing todo without license/key
         */
        WAF_log_debug("Failed to locate license in provided configuration file. Return default config.");
        return WAF_get_default_configuration();
    }

    if(!array_key_exists(WAF_TIMEOUT,$ini_array)){
        $ini_array[WAF_TIMEOUT]=WAF_REQUEST_TIMEOUT;
    }

    if(!array_key_exists(WAF_PORT,$ini_array)){
        $ini_array[WAF_PORT]=WAF_NODE_PORT;
    }

    if(!array_key_exists(WAF_MODE,$ini_array)){
        WAF_log_debug("Failed to locate configured WAF mode. Using passive mode");
        $ini_array[WAF_MODE]=WAF_MODE_OFF;
    }

    if(!array_key_exists(WAF_CONTROL_ADDR,$ini_array)){
        WAF_log_debug("Failed to locate configured control server.");
        $ini_array[WAF_CONTROL_ADDR]="";
    }

    WAF_log_debug($ini_array);
    return $ini_array;
}



/**
 * @brief       translate input WAF protection mode to predefined value
 * @param[in]   mode - value to translate
 * @return      translated value
 */
function WAF_get_mode( $mode ){
    $m = strtolower($mode);
    if($mode === WAF_MODE_ON){
        return WAF_MODE_ON;
    }else if($mode === WAF_MODE_PASSIVE){
        return WAF_MODE_PASSIVE;
    }else{
        return WAF_MODE_OFF;
    }
    return WAF_MODE_OFF;
}


/**
 * @brief       send WAF request using CURL interface
 * @param[in]   url       - URL to send request
 * @param[in]   params    - request params
 * @param[in]   config    - WAF configuration
 * @return      on success return's verdict received from WAF, oposite NULL
 */
function WAF_curl_send_verdict_request( $url, $params, $qtr_waf_config)
{
    WAF_log_debug("Sending curl request to $url");

    $qtr_waf_data_string = json_encode($params);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $qtr_waf_data_string);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($qtr_waf_data_string))
    );


    $output = curl_exec($ch);

    if($errno = curl_errno($ch)) {
        $qtr_waf_error_message = curl_strerror($errno);
        WAF_log_debug("CURL error $errno {$qtr_waf_error_message}");
    }

    WAF_log_debug($output);
    curl_close($ch);
    return $output;
}


/**
 * @brief   sends verdict request using default PHP interface
 * @param[in] url       - URL to send request
 * @param[in] params    - request params
 * @param[in] config    - WAF configuration
 * @return  on success return's verdict received from WAF, oposite NULL
 */
function WAF_raw_send_verdict_request( $url, $params, $qtr_waf_config)
{
    $qtr_waf_data_string = json_encode($params);
    $options = array(
        'http' => array(
            'method'  => 'POST',
            'header'  => "Content-type: application/json\r\n" . 
                         "Content-Length: " . strlen($qtr_waf_data_string) . "\r\n",
            'content' => $qtr_waf_data_string,
            'timeout' => $qtr_waf_config[WAF_TIMEOUT],
            'follow_location' => 1,
            'ignore_errors' => 1
        )
    );

    WAF_log_debug($options);
    $context  = stream_context_create($options);
    WAF_log_debug("Sending raw request to $url. JSON '$qtr_waf_data_string'");
    $qtr_waf_result = @file_get_contents($url, false, $context);
    return $qtr_waf_result;
}


/**
 * @brief   sends verdict request
 * @param[in] url       - URL to send request
 * @param[in] params    - request params
 * @param[in] config    - WAF configuration
 * @return  on success return's verdict received from WAF, oposite NULL
 */
function WAF_send_verdict_request( $url, $params, $qtr_waf_config)
{
    $use_curl = function_exists('curl_exec');
    //$use_curl = false;
    if($use_curl){
        return WAF_curl_send_verdict_request($url,$params, $qtr_waf_config);
    }
    WAF_log_debug("CURL is not available. Using RAW request");
    return WAF_raw_send_verdict_request($url,$params, $qtr_waf_config);
}


/**
 * @brief       test nonce token using CURL interface
 * @param[in]   control   - URL to send request
 * @param[in]   license   - licensed user ID
 * @param[in]   nonce     - nonce value to test
 * @return      on success return's TRUE (nonce valid) , oposite FALSE
 */
function WAF_curl_send_nonce_request( $control, $qtr_waf_license, $nonce)
{
    $nonce_url = "https://$control:443/nonce/$qtr_waf_license/$nonce";
	WAF_log_debug("Sending curl request to $nonce_url");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $nonce_url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($qtr_waf_data_string))
    );

    $output = curl_exec($ch);

    if($errno = curl_errno($ch)) {
        $qtr_waf_error_message = curl_strerror($errno);
        WAF_log_debug("CURL error $errno {$qtr_waf_error_message}");
        return False;
    }

	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    WAF_log_debug("HTTP response from $nonce_url is $httpcode");
    curl_close($ch);
	return ( $httpcode == 200 )?TRUE:FALSE;
}


/**
 * @brief   sends nonce request using default PHP interface
 * @param[in]   control   - URL to send request
 * @param[in]   license   - licensed user ID
 * @param[in]   nonce     - nonce value to test
 * @return      on success return's TRUE (nonce valid) , oposite FALSE
 */
function WAF_raw_send_nonce_request( $control, $qtr_waf_license, $nonce)
{
    WAF_log_debug("Sending raw nonce request to $url.");
    $nonce_url = "https://$control:443/nonce/$qtr_waf_license/$nonce";
    $qtr_waf_result = @file_get_contents($nonce_url);
	return (strpos($qtr_waf_result,"200") === FALSE)?FALSE:TRUE;
}


/**
 * @brief   sends nonce request
 * @param[in]   control   - URL to send request
 * @param[in]   license   - licensed user ID
 * @param[in]   nonce     - nonce value to test
 * @return      on success return's TRUE (nonce valid) , oposite FALSE
 */
function WAF_send_nonce_request( $control, $qtr_waf_license, $nonce)
{
    $use_curl = function_exists('curl_exec');
    //$use_curl = false;
    if($use_curl){
        return WAF_curl_send_nonce_request($control, $qtr_waf_license, $nonce);
    }
    WAF_log_debug("CURL is not available. Using RAW request");
    return WAF_raw_send_nonce_request($control, $qtr_waf_license, $nonce);
}



function send_stats(){
    die;
}

function get_last_verdict_time(){
    $fname = dirname(__FILE__) . DIRECTORY_SEPARATOR . WAF_VERDICT_TIME;
    $qtr_waf_error = "0.000";
    $fd = @fopen($fname,"r");
    if($fd == FALSE){
        return $qtr_waf_error;
    }
    $rc = fread($fd, filesize($fname));
    fclose($fd);
    if($rc == FALSE){
        return $qtr_waf_error;
    }
    return $rc;
}

function send_config_and_stats(){
    global $qtr_waf_config, $WAF_CONNECTOR_VERSION;
    $qtr_waf_config["version"] = $WAF_CONNECTOR_VERSION;
    $stats = array();
    $stats["vtime"] = get_last_verdict_time();
    header('Content-Type: application/json');
    $output = array("stats" => $stats, "config" => $qtr_waf_config);
    $output = json_encode($output);
    WAF_log_debug("Sending output: $output");
    echo $output . "\r\n";
    die;
}


function send_config(){
    global $WAF_CONNECTOR_VERSION, $qtr_waf_config_file;
    $qtr_waf_config  = WAF_load_config($qtr_waf_config_file); 
    $qtr_waf_config["version"] = $WAF_CONNECTOR_VERSION;
    header('Content-Type: application/json');
    $output = array("config" => $qtr_waf_config);
    $output = json_encode($output);
    WAF_log_debug("Sending output: $output");
    echo $output . "\r\n";
    die;
}


/**
 * @brief       string decoder procedure
 * @param[in]   $string - string to decode
 * @param[in]   $skey - decoder key to use
 * @return      decoded string (or empty)
 */
function WAF_decode_string($string, $skey)
{
    WAF_log_debug("Key: $skey, str: $string");
    $output = "";
    $j = 0;
    $max = strlen($skey);
    $chars = explode (".", $string);
    foreach($chars as $char) {
        // char is string of the asci code of the required character
        $iint = intval($char);
        $jint = intval($skey[$j], 16);
        $j = ($j + 1) % $max;
        $ch = $iint ^ $jint;
        $output .= chr($ch); 
    }

    WAF_log_debug("Output: $output");
    return $output;
}


function WAF_serialize_array($arr, $prefix=NULL)
{
    $ret_str = "";
    foreach ($arr as $key => $value)
    {
        $svalue = $value;
        if(is_array($svalue)){
            $svalue = WAF_serialize_array($svalue);
        }

        if( $prefix ){
            $ret_str .= "$prefix.$key&$svalue;";
        }else{
            $ret_str .= "$key&$svalue;";
        }
    }

    return $ret_str;
}


/**
 * @brief       decode control request sent by control server
 * @param[in]   $payload - command payload to decode
 * @param[in]   $skey - decoder key 
 * @return      NULL on error, or decoded request form
 */
function WAF_decode_controller_request($payload, $skey)
{
    $decoded = WAF_decode_string($payload, $skey);
    if(!$decoded){
        WAF_log_debug("Failed to decode provided payload");
        return NULL;
    }

    $qtr_waf_request = json_decode($decoded, true);
    if(!$qtr_waf_request){
        WAF_log_debug("Failed to parse JSON");
        return NULL;
    }

    if(!array_key_exists("userid", $qtr_waf_request)){
        WAF_log_debug("Provided invalid userid");
        return NULL;
    }

    if(strcmp($qtr_waf_request["userid"], $skey) != 0 ){
        WAF_log_debug("Provided invalid userid");
        return NULL;
    }

    if(!array_key_exists("control", $qtr_waf_request)){
        WAF_log_debug("Provided invalid control request");
        return NULL;
    }

    return $qtr_waf_request;
}


/*******************************************************
 *******************************************************
 *
 *                 Connector entry point 
 *
 *******************************************************
 *******************************************************/
WAF_log_debug("Loading configuration from $qtr_waf_config_file");

$qtr_waf_config = WAF_load_config( $qtr_waf_config_file );
$mode   = WAF_get_mode($qtr_waf_config[WAF_MODE]);

#echo "URL " .  $_SERVER["REDIRECT_URL"] . "\r\n";
#echo "URI " .  $_SERVER["REQUEST_URI"] . "\r\n";
#echo "License: " . $qtr_waf_config[WAF_LICENSE] . "\r\n";

#WAF_log_debug($_POST);
#WAF_log_debug($_GET);

/*
 * Run cure every N HTT requests
 */
WAF_run_cure(5);

if(array_key_exists(WAF_CONTROL, $_POST))
{
    /*
     * Looks like control request, need to validate it
     */
    $curr_remote_ip = WAF_safe_get_value($_SERVER, 'REMOTE_ADDR', "NULL");
    $conf_remote_ip = gethostbyname($qtr_waf_config[WAF_CONTROL_ADDR]);

    if( $conf_remote_ip == $qtr_waf_config[WAF_CONTROL_ADDR] )
    {
        WAF_log_debug("ERROR gethostbyname failed to get IP of control host");
    }

    if( $WAF_CONTROL_TEST_SOURCE and strcmp($curr_remote_ip, $conf_remote_ip) ){
        /*
         * Control request could be sent only from preconfigured control server
         */
        WAF_log_debug("Control request sent from wrong server $curr_remote_ip, configured $conf_remote_ip " . $qtr_waf_config[WAF_CONTROL_ADDR]);
        /*
         * request sent from invalid host
         */
        http_response_code(403);
        exit('403 Forbidden');
    }
	/*
     * decode recieved messages
     */
    $qtr_waf_request = WAF_decode_controller_request($_POST[WAF_CONTROL], $qtr_waf_config[WAF_LICENSE]);

    if(!$qtr_waf_request){
        http_response_code(403);
        exit('403 Forbidden');
    }
    
    if(!array_key_exists("control", $qtr_waf_request)){
        http_response_code(400);
        exit('400 Bad Request');
    }

    WAF_log_debug("Control request: " . var_export($qtr_waf_request, true)); 

    if( $WAF_CONTROL_TEST_SOURCE == FALSE )
    {
        /*
         * We cannot/unable to test control IP, 
         * check nonce to verify this message sent from configured control
         */
        if(!array_key_exists("nonce", $qtr_waf_request)){
            WAF_log_debug("Provided invalid nonce value");
            http_response_code(403);
            exit('403 Forbidden');
        }


		if(!WAF_send_nonce_request($qtr_waf_config[WAF_CONTROL_ADDR],$qtr_waf_config[WAF_LICENSE], $qtr_waf_request["nonce"])){
            WAF_log_debug("Failed to validate nonce-token $nonce");
            http_response_code(403);
            exit('403 Forbidden');
		}
    }
    
    switch($qtr_waf_request["control"]){
        case "all":
            return send_config_and_stats();
        case "config":
            return send_config();
        case "stats":
            return send_stats();
        case "turnon":
            WAF_log_debug("Turning ON WAF adapter");
            $qtr_waf_config[WAF_MODE] = WAF_MODE_ON;
            WAF_store_config($qtr_waf_config, $qtr_waf_config_file);
            return send_config();
        case "turnoff":
            WAF_log_debug("Turning OFF WAF adapter");
            $qtr_waf_config[WAF_MODE] = WAF_MODE_OFF;
            WAF_store_config($qtr_waf_config, $qtr_waf_config_file);
            return send_config();
        case "learning":
            WAF_log_debug("Setting learning mode");
            $qtr_waf_config[WAF_MODE] = WAF_MODE_PASSIVE;
            WAF_store_config($qtr_waf_config, $qtr_waf_config_file);
            return send_config();
    }

    http_response_code(400);
    exit('400 Bad Request');
}


WAF_log_debug("WAF mode $mode");

/*
 * non-deterministic function to clean old logs once in a while
 */
WAF_CleanLogs();

if( $mode === WAF_MODE_ON || $mode === WAF_MODE_PASSIVE )
{
    /*
     * get user ID from waf.ini
     */
    $qtr_waf_license=$qtr_waf_config[WAF_LICENSE];
    /*
     * build request path
     */
    $qtr_waf_proto="http";
    $qtr_waf_node_address = $qtr_waf_config[WAF_ADDRESS];
    if(array_key_exists(WAF_PORT,$qtr_waf_config)){
        $qtr_waf_port = $qtr_waf_config[WAF_PORT];
        $qtr_waf_node_address="$qtr_waf_node_address:$qtr_waf_port";
        if(intval($qtr_waf_port) === 443 ){
            $qtr_waf_proto = "https";
        }
    }
    /*
     * build WAF query to get verdict
     */
    $WAF_address = "$qtr_waf_node_address/waf/v1/$qtr_waf_license/verdict";

    if( strpos($WAF_address,"http") === FALSE )
    {
        $WAF_address = "$qtr_waf_proto://$WAF_address";
    }

    WAF_log_debug("Connecting to $WAF_address");

    $qtr_waf_domain = WAF_safe_get_value($_SERVER, "SERVER_NAME", "[SERVER_NAME]");

    if( filter_var($qtr_waf_domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) and
        array_key_exists(WAF_DEFAULT_DOMAIN, $qtr_waf_config) and strlen($qtr_waf_config[WAF_DEFAULT_DOMAIN]) > 4 )
    {
        /*
         * this is IP based request, set it to default domain name
         */
        $qtr_waf_domain = $qtr_waf_config[WAF_DEFAULT_DOMAIN];
    }

    $redirect_url = $_SERVER["REQUEST_URI"];
    if( array_key_exists("REDIRECT_URL", $_SERVER) ){
        $redirect_url = $_SERVER["REDIRECT_URL"];
    }

    $agent = "undet";
    if( array_key_exists("HTTP_USER_AGENT", $_SERVER) ){
        $agent = $_SERVER["HTTP_USER_AGENT"];
    }

    /*
     * Build WAF request
     */
    $WAF_query = array(
        'user'              => $qtr_waf_license,
        'connector_version' => $WAF_CONNECTOR_VERSION,
        'domain'            => $qtr_waf_domain,
        'query_string'      => WAF_safe_get_value($_SERVER, "QUERY_STRING", ""),
        'visitor_ip'        => WAF_safe_get_value($_SERVER, "REMOTE_ADDR", "[REMOTE_ADDR]"),
        'http_method'       => WAF_safe_get_value($_SERVER, "REQUEST_METHOD", "GET"),
        'agent'             => $agent,
        'request_url'       => $redirect_url,
        'request_uri'       => WAF_safe_get_value($_SERVER, "REQUEST_URI", ""),
        'target_file_md5'   => "undef",
        'request_params'    => "",
    );

    WAF_log_debug($WAF_query);

    if( $_SERVER["REQUEST_METHOD"]  == 'POST' )
    {
        //append post fields as well
        //$WAF_query['request_params'] = json_encode($_POST);

        $WAF_query['request_params'] = WAF_serialize_array($_POST);

        if( count($_FILES) > 0 )
        {
            /*
             * file upload attempt
             */
            $WAF_query['request_params'] .= ' $_FILES:';
            $str = preg_replace('/[\r\n]/','', print_r($_FILES, TRUE));
            $str = preg_replace('/\s+/',' ',$str);
            $WAF_query['request_params'] .= "\t" . $str;
        }
    }
    /*
    else
    {
        //append GET fields as well      
        $WAF_query['request_params'] = json_encode($_GET);
    }*/

    WAF_log_access( $WAF_query );

    if( rand() % 5 == 0){
        $stime = microtime(TRUE);
        $qtr_waf_result = WAF_send_verdict_request($WAF_address,$WAF_query, $qtr_waf_config);
        $etime = microtime(TRUE);
        /*
         * Write out how many time it took to get verdict response
         */
        $fd = @fopen(dirname(__FILE__) . DIRECTORY_SEPARATOR . WAF_VERDICT_TIME, "w+");
        if($fd != FALSE )
        {
            if (flock($fd, LOCK_EX))
            {  
                // acquire an exclusive lock
                fwrite($fd,sprintf("%f",$etime - $stime));
                fflush($fd);
            }
            fclose($fd);
        }
    }else{
        $qtr_waf_result = WAF_send_verdict_request($WAF_address,$WAF_query, $qtr_waf_config);
    }

    if ($qtr_waf_result == false)
    {
        /* 
         * Handle error, no communication with WAF
         */
        $qtr_waf_error = sprintf(   "ERROR %s [%s %s%s] no verdict returned from WAF",
                                    $WAF_address,         
                                    WAF_safe_get_value($_SERVER, "REQUEST_METHOD", "GET"),
                                    WAF_safe_get_value($_SERVER, "SERVER_NAME", "[SERVER_NAME]"),
                                    WAF_safe_get_value($_SERVER, "REQUEST_URI", "[REQUEST_URI]"));

        WAF_log_error($qtr_waf_error);
        WAF_log_debug("Request failed. $qtr_waf_error");
    }
    else
    {
        $qtr_waf_attack = FALSE;
        $qtr_waf_action = NULL;
        $qtr_waf_redirect = "";
        $qtr_waf_rule = "";

        WAF_log_debug($qtr_waf_result); 

        $qtr_waf_data = json_decode($qtr_waf_result, TRUE);

        WAF_log_debug($qtr_waf_data); 

        if( array_key_exists("action", $qtr_waf_data ) ){
            $qtr_waf_action = $qtr_waf_data["action"];
        }

        if( array_key_exists("rule", $qtr_waf_data ) ){
            $qtr_waf_rule = $qtr_waf_data["rule"];
        }

        if( array_key_exists("redirect", $qtr_waf_data ) ){
            $qtr_waf_redirect = $qtr_waf_data["redirect"];
        }

        /*
         * if attack detected
         * Supported verdicts
         *      BLOCK - request should be blocked
         *        DROP  - request should be dropped with no response (TODO - need to verify how to do that in PHP)
         *        REDIRECT - traffic should be redirected to another location
         *      PASS  - request is allowed to be passed
         *      NOACTION - similar to PASS, traffic could be passed
         *        NOLICENSE - given user:website pair is not licensed
         */
        
        if( $QTR_WAF_DEBUG ){
            /*
             * If DEBUG set, write log for any verdict
             */
            WAF_log_verdict($qtr_waf_action,
                            $qtr_waf_rule,
                            $qtr_waf_redirect);
        }

        switch( $qtr_waf_action )
        {
            case "BLOCK":
                if( $QTR_WAF_DEBUG == 0 )
                {
                    WAF_log_verdict($qtr_waf_action, 
                                    $qtr_waf_rule,
                                    $qtr_waf_redirect); 
                }
                
                http_response_code(403);
                
                header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
                exit('403 Forbidden');
            case "DROP":
                if( $QTR_WAF_DEBUG == 0 )
                { 
                    WAF_log_verdict($qtr_waf_action,
                                    $qtr_waf_rule,
                                    $qtr_waf_redirect); 
                }

                http_response_code(503);
                
                header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable', true, 503);
                exit('503 Service Unavailable');
            case "REDIRECT":
                if( $QTR_WAF_DEBUG == 0 )
                { 
                    WAF_log_verdict($qtr_waf_action,
                                    $qtr_waf_rule,
                                    $qtr_waf_redirect); 
                }
                http_response_code(301);
                header("Location: " . $qtr_waf_redirect);
                exit("301 Moved Permanently");
            case "NOLICENSE":
                // TODO - decide what we are doing here
            case "PASS":
            case "NOACTION":
                // nothing to do here
                break;
            default:
                break;
        }
    }
}
?>
